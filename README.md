This Project implements for Banker algorithm a Server-Client platform.

This Project was done for the OS course given by Liam Paull http://liampaull.ca/courses/ift2245

Contributor:
Jad Yammine
Moncef Fayjhi

Project Info:

Pour ce TP, vous devez impl´ementer en C l’algorithme du banquier, un algorithme
qui permet de g´erer l’allocation des diff´erents types des ressources, tout
en ´evitant les interblocages (deadlocks). Le code fourni impl´emente un mod`ele
de client-serveur qui utilise les prises (sockets) comme moyen de communication.
D’un cˆot´e l’application serveur est analogue `a un syst`eme d’exploitation qui
g`ere l’allocation des ressources, comme par exemple la m´emoire, le disque dur
ou les imprimantes. Ce serveur re¸coit simultan´ement plusieurs demandes de
ressources des diff´erents clients `a travers des connexions. Pour chaque connexion/requˆete,
le serveur doit d´ecider quand les ressources peuvent ˆetre allou´ees
au client, de fa¸con `a ´eviter les interblocages en suivant l’algorithme du banquier.
De l’autre cˆote, l’application client simule l’activit´e de plusieurs clients dans
diff´erents fils d’ex´ecution (threads). Ces clients peuvent demander des ressources
si elles sont disponibles ou lib´erer des ressource qu’ils d´etiennent `a ce moment.
Ce TP vous permettra de mettre en pratique quatre sujets du cours diff´erents:
• Fils d’ex´ecution multiples (multithreading).
• Pr´evention des s´equencements critiques (race conditions).
• Evitement d’interblocages (deadlock avoidance). ´
• Communication entre processus via des prises (sockets).
3 Mise en place
Des fichiers vous sont fournis pour vous aider `a commencer ce TP. On vous
demande de travailler `a l’int´erieur de la structure d´ej`a fournie.
Les deux applications, client et serveur, utilisent les librairies du standard
POSIX pour l’impl´ementation des structures dont vous allez avoir besoin, notamment
les sockets, les threads, les semaphores et les mutex. Ces librairies
sont par defaut install´ees sur une installation du syst`eme GNU/Linux.
3.1 Makefile
Pour vous faciliter le travail, le fichier GNUmakefile de l’archive vous permet
d’utiliser les commandes suivantes:
• make ou make all: Compile les deux applications.
• make release: Archive le code dans un tar pour la remise.
• make run: Lance le client et le serveur ensembles.
• make run-client: Lance le client.
• make run-server: Lance le serveur.
2
• make run-valgrind-client: Lance le client dans valgrind.
• make run-valgrind-server: Lance le serveur dans valgrind.
• make clean: Nettoie le dossier build.
4 Protocole client serveur
Le protocole de communication entre le client et le serveur est constitu´e de
commandes et de r´eponses qui sont chacune une ligne de texte au format utf-8
(et termin´ee par le caract`ere ASCII 10, line-feed).
BEG nb resources Configure le nombre de ressources
PRO rsc0 rsc1 ... Provisionne les ressources
END Termine l’ex´ecution du serveur
INI tid max0 max1 ... Annonce client avec son usage maximum
REQ tid rsc0 rsc1 ... Requˆete de ressources
CLO tid Annonce la fin du client
Figure 1: Requˆetes du client
ACK Commande ex´ecut´ee avec succ`es
ERR msg Commande invalide, msg explique pourquoi
WAIT sec Demande au client d’attendre sec secondes
Figure 2: R´eponses du serveur
Le protocole est dirig´e par le client qui fait des requˆetes au serveur. Les
r´eponses du serveur sont toujours ACK, ERR, ou WAIT, comme montr´e `a la
Figure 2.
Les requˆetes, montr´ees `a la Figure 1, se divisent en deux parties:
• les commandes globales utilis´ees au tout d´ebut pour configurer le serveur
puis `a la fin pour le terminer.
• Les commandes par client (o`u chaque client est en fait un thread du programme
tp2 client).
Donc apr`es avoir lanc´e le serveur, le programme client le configure par exemple
avec:
BEG 5
PRO 10 5 3 23 1
Suite `a cela, les diff´erents threads du client peuvent chacun ouvrir une connection
et y envoyer leurs requˆetes, qui pourraient alors avoir la forme suivante
pour le thread client avec le id 332:
3
INI 332 1 2 1 10 0
REQ 322 1 0 0 1 0 0
REQ 332 0 2 0 0 0
REQ 332 0 0 -1 0 0
REQ 332 0 -2 0 0 0
CLO 332
A remarquer que pour lib´erer des ressources, il suffit d’utiliser des quantit´es `
n´egatives dans une requˆete.
A remarquer que si un client est mis en attente par un ` WAIT, il ne fait que
r´ep´eter la mˆeme requˆete apr`es l’attente jusqu’`a recevoir un ACK.
C’est une erreure s’il reste des ressources occup´ees lors du CLO ou des clients
encore connect´es lors du END. A la fin, le serveur effectue alors l’impression du `
journal `a l’int´erieur d’un fichier et le client fait de mˆeme `a la r´eception de ACK.
Le serveur peut alors se fermer ainsi que le client.
4.1 Remise
Pour la remise, vous devez remettre tous les fichiers C ainsi que le rapport.tex
dans une archive tar par la page StudiUM du cours. Vous pouvez utiliser make
release pour cr´eer l’archive en question.
5 D´etails
• La note sera divis´ee comme suit: 20% pour chacune des 4 sujets (multithreading,
race conditions, deadlock avoidance, et sockets), et 20% pour
le rapport.
• Tout usage de mat´eriel (code ou texte) emprunt´e `a quelqu’un d’autre
(trouv´e sur le web, ...) doit ˆetre dˆument mentionn´e, sans quoi cela sera
consid´er´e comme du plagiat.
• Le code ne doit en aucun cas d´epasser 80 colonnes.
• V´erifiez la page web du cours, pour d’´eventuels errata, et d’autres indications
suppl´ementaires.
• La note sera bas´ee d’une part sur des tests automatiques, d’autre part sur
la lecture du code, ainsi que sur le rapport. Le crit`ere le plus important,
et que votre code doit se comporter de mani`ere correcte. Ensuite, vient
la qualit´e du code: plus c’est simple, mieux c’est. S’il y a beaucoup
de commentaires, c’est g´en´eralement un symptˆome que le code n’est pas
clair; mais bien sˆur, sans commentaires le code (mˆeme simple) est souvent
incompr´ehensible. L’efficacit´e de votre code est sans importance, sauf s’il
utilise un algorithme vraiment particuli`erement ridiculement inefficace.
• Les fuites de m´emoire sont p´enalis´ees.
4