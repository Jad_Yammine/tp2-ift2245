/* This `define` tells unistd to define usleep and random.  */
#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "client_thread.h"

// Socket library
#include <netdb.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define SERVER_SPACE = "                        "

enum {
    NUL = '\0'
};

bool accepting_connections = true;

struct sockaddr_in addr;

int*provisioned_resources = NULL;

// Variable d'initialisation des threads clients.
unsigned int count = 0;

#define BEG_ID 9999
#define PRO_ID 9998

// Variable du journal.
// Nombre de requête acceptée (ACK reçus en réponse à REQ)
unsigned int count_accepted = 0;

// Nombre de requête en attente (WAIT reçus en réponse à REQ)
unsigned int count_on_wait = 0;

// Nombre de requête refusée (REFUSE reçus en réponse à REQ)
unsigned int count_invalid = 0;

// Nombre de client qui se sont terminés correctement (ACC reçu en réponse à END)
unsigned int count_dispatched = 0;

// Nombre total de requêtes envoyées.
unsigned int request_sent = 0;

struct client_profile {
    int client_id;
    int current_nb_request;
    int *r_max_usage;
    int *r_taken;
};



struct client_profile *client_rep;

void print_clients() {
    printf("Printing registered clients:\n");
    for (int i = 0; i < num_clients; i++) {
        printf("Client id: >%d<\n", client_rep[i].client_id);
        print_resources(client_rep[i].r_max_usage);

    }
}

void print_resources(int* x) {
    printf("Printing ressources :\n");
    for (int i = 0; i < num_resources; i++) {
        printf("Ressource # %d contains: %d \n", i, x[i]);
    }
}
// Vous devez modifier cette fonction pour faire l'envoie des requêtes
// Les ressources demandées par la requête doivent être choisies aléatoirement
// (sans dépasser le maximum pour le client). Elles peuvent être positives
// ou négatives.
// Assurez-vous que la dernière requête d'un client libère toute les ressources
// qu'il a jusqu'alors accumulées.

char*
send_request(int socket_fd, char* req) {
    char *buff = malloc(1024 * sizeof (char));
    send(socket_fd, req, strlen(req), 0);
    read(socket_fd, buff, (1024 * sizeof (char)));
    return buff;
}


//This will generate a random number between -x and x

int get_signed_rand(int x) {
    return (rand() % ((x * 2) + 1) - x) / 5;
}

//This will generate a positive number smaller then x

int get_unsigned_rand(int x) {
    return (rand() % ((x + 1)));
}

void run_client(int id) {

    int cs = ct_open_THREAD_socket();
    for (int current_req = 0; current_req < num_request_per_client; current_req++) {
        //int* one_request = malloc(num_resources*sizeof(int));

        client_rep[id].current_nb_request++;
        char* one_req_str = malloc(100 * sizeof (char));
        sprintf(one_req_str, "REQ %d", id);
        for (int i = 0; i < num_resources; i++) {
            int one_resource_req = get_signed_rand(client_rep[id].r_max_usage[i]);
            sprintf(one_req_str, "%s %d", one_req_str, one_resource_req);
        }

        int size_buff = 0;
        bool send_again = false;
        do {
            char *buff2 = malloc(1024 * sizeof (char));
            buff2 = send_request(cs, one_req_str);
            size_buff = strlen(buff2);
            if (buff2[0] == 'A' && buff2[1] == 'C' && buff2[2] == 'K') {
                printf("Client thread with id >>%d<< REQ # >>%d<< ACCEPTED!!!!!!!\n", id, current_req);
                count_accepted++;
            } else if (buff2[0] == 'W' && buff2[1] == 'A' && buff2[2] == 'I') {
                int w_t = atoi(strtok(buff2 + 5, " "));
                printf("Client Server INI ACK ON WAIT\n");
                send_again = true;
                count_on_wait++;
                sleep(w_t);
            } else if (buff2[0] == 'E' && buff2[1] == 'R' && buff2[2] == 'R') {
                char* msg = strtok(buff2 + 4, " ");
                send_again = false;
                printf("Client INI REFUSED: %s\n", msg);
                count_invalid++;
            }
            free(buff2);
            if (size_buff == 0) {
                close(cs);
                cs = ct_open_THREAD_socket();
            }
        } while (send_again == true || (size_buff == 0));
        free(one_req_str);
    }
    close(cs);
}

void init_client(int id, int cs) {

    struct client_profile x;
    x.client_id = id;
    x.r_max_usage = malloc(num_resources * sizeof (int));
    char* ini_str = malloc(10 * num_resources * sizeof (char));
    sprintf(ini_str, "INI %d", id);
    for (int i = 0; i < num_resources; i++) {
        x.r_max_usage[i] = get_unsigned_rand(provisioned_resources[i]);
        sprintf(ini_str, "%s %d", ini_str, x.r_max_usage[i]);
    }
    bool send_again = false;
    do {
        char *ini_buff = malloc(1024 * sizeof (char));
        ini_buff = send_request(cs, ini_str);
        if (ini_buff[0] == 'A' && ini_buff[1] == 'C' && ini_buff[2] == 'K') {
            printf("Client thread with id >>%d<< INI ACCEPTED!\n", id);
            x.current_nb_request = -1;
            x.r_taken = malloc(num_resources * sizeof (int));
            client_rep[id] = x;
            send_again = false;
        } else if (ini_buff[0] == 'W' && ini_buff[1] == 'A' && ini_buff[2] == 'I') {
            int w_t = atoi(strtok(ini_buff + 5, " "));
            printf("Client Server INI ACK ON WAIT\n");
            send_again = true;
            sleep(w_t);
        } else if (ini_buff[0] == 'E' && ini_buff[1] == 'R' && ini_buff[2] == 'R') {
            char* msg = strtok(ini_buff + 4, " ");
            send_again = false;
            printf("Client INI REFUSED: %s\n", msg);
            free(msg);
        }
        free(ini_buff);
    } while (send_again == true);
    free(ini_str);
}

void clo_client(int id, int cs) {

    bool send_again = false;
    do {
        char* clo_str = malloc(10 * num_resources * sizeof (char));
        sprintf(clo_str, "CLO %d ", id);
        char *buff = malloc(1024 * sizeof (char));
        buff = send_request(cs, clo_str);
        printf("Client thread with id >>%d<< CLO >>%s<<!\n", id, buff);
        if (buff[0] == 'A' && buff[1] == 'C' && buff[2] == 'K') {
            printf("Client thread with id >>%d<< CLO ACCEPTED!\n", id);
            count_dispatched++;
            send_again = false;
        } else if (buff[0] == 'E' && buff[1] == 'R' && buff[2] == 'R') {
            char* msg = strtok(buff + 4, " ");
            //send_again = true;
            printf("Client CLO REFUSED: %s\n", msg);
            free(msg);
            send_again = false;
        } else {
            send_again = true;
        }
        free(buff);
        free(clo_str);
    } while (send_again == true);
}

void
startup() {
    char *str_resources = malloc(num_resources * sizeof (char)*100);
    for (int i = 0; i < num_resources; i++) {
        sprintf(str_resources, "%s %d", str_resources, provisioned_resources[i]);
    }
    char beg[6] = {0};
    sprintf(beg, "BEG %d %d", num_resources, num_clients);
    client_rep = malloc(num_clients * (2 * sizeof (int) + 2 * num_resources * sizeof (int)));
    char *buff = malloc(1024 * sizeof (char));
    buff = send_request(client_socket, beg);
    if (buff[0] == 'A' && buff[1] == 'C' && buff[2] == 'K') {
        printf("Client Server BEG ACK ACCEPTED.\n");
    } else {
        printf("Client Server BEG ACK REFUSED\n");
    }
    free(buff);
    char pro[1000] = {0};
    sprintf(pro, "PRO%s", str_resources);
    char *buff2 = malloc(1024 * sizeof (char));
    buff2 = send_request(client_socket, pro);
    if (buff2[0] == 'A' && buff2[1] == 'C' && buff2[2] == 'K') {
        printf("Client Server PRO ACK ACCEPTED\n");
    } else {
        printf("Client Server PRO ACK REFUSED\n");
    }
    free(buff2);
    free(str_resources);
}

void *
ct_code(void * param) {

    client_thread *ct = param;
    int cs = ct_open_THREAD_socket();
    init_client(ct->id, cs);
    close(cs);
    
    
    run_client(ct->id);
    
    
    int cs2 = ct_open_THREAD_socket();
    clo_client(ct->id, cs2);
    close(cs2);
    /* Attendre un petit peu (0s-0.1s) pour simuler le calcul.  */
    //usleep (random () % (100 * 1000));
    /* struct timespec delay;
     * delay.tv_nsec = random () % (100 * 1000000);
     * delay.tv_sec = 0;
     * nanosleep (&delay, NULL); */
    // pthread_join(ct->pt_tid, NULL);

    return NULL;
}


//
// Vous devez changer le contenu de cette fonction afin de régler le
// problème de synchronisation de la terminaison.
// Le client doit attendre que le serveur termine le traitement de chacune
// de ses requêtes avant de terminer l'exécution.
//

void
ct_wait_server() {

    // TP2 TODO: IMPORTANT code non valide.

    sleep(5);

    // TP2 TODO:END

}

void
ct_init(client_thread * ct) {
    ct->id = count++;
}

void
ct_create_and_start(client_thread * ct) {
    pthread_attr_init(&(ct->pt_attr));
    pthread_create(&(ct->pt_tid), &(ct->pt_attr), &ct_code, ct);
    pthread_detach(ct->pt_tid); //source potentielle de bugs,mis en commentaire 
}

//
// Affiche les données recueillies lors de l'exécution du
// serveur.
// La branche else ne doit PAS être modifiée.
//

void
st_print_results(FILE * fd, bool verbose) {
    if (fd == NULL)
        fd = stdout;
    if (verbose) {
        fprintf(fd, "\n---- Résultat du client ----\n");
        fprintf(fd, "Requêtes acceptées: %d\n", count_accepted);
        fprintf(fd, "Requêtes : %d\n", count_on_wait);
        fprintf(fd, "Requêtes invalides: %d\n", count_invalid);
        fprintf(fd, "Clients : %d\n", count_dispatched);
        fprintf(fd, "Requêtes envoyées: %d\n", request_sent);
    } else {
        fprintf(fd, "%d %d %d %d %d\n", count_accepted, count_on_wait,
                count_invalid, count_dispatched, request_sent);
    }
}

void
ct_open_socket() {

    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket < 0) {
        printf("client socket opening error!!!!!\n");
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(2018);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    // Connect client socket to server.
    if (connect(client_socket, (struct sockaddr *) &(addr), sizeof (addr))) {
        printf("client socket connection error!!!!!\n");
    }

}

int
ct_open_THREAD_socket() {

    int cs = -1;
    cs = socket(AF_INET, SOCK_STREAM, 0);
    if (cs < 0) {
        printf("client socket opening error!!!!!\n");
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(2018);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    // Connect client socket to server.
    if (connect(cs, (struct sockaddr *) &(addr), sizeof (addr))) {
        printf("client socket connection error!!!!!\n");
        exit(0);
    } else {
        return cs;
    }
}





