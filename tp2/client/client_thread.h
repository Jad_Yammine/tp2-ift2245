#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <unistd.h>
int num_clients;
/* Port TCP sur lequel le serveur attend des connections.  */
int port_number;

/* Nombre de requêtes que chaque client doit envoyer.  */
int num_request_per_client;

/* Nombre de resources différentes.  */
int num_resources;

/* Quantité disponible pour chaque resource.  */
int *provisioned_resources;
int client_socket;
pthread_mutex_t mutex;

typedef struct client_thread client_thread;
struct client_thread
{
  unsigned int id;
  pthread_t pt_tid;
  pthread_attr_t pt_attr;
};
void ct_open_socket();
void startup();
void print_resources(int* x);
void print_clients();
void ct_init (client_thread *);
void ct_create_and_start (client_thread *);
void ct_wait_server ();
void test();
void st_print_results (FILE *, bool);

int ct_open_THREAD_socket();
void wait_for_ACK(int socket_fd,int id);
#endif // CLIENTTHREAD_H
