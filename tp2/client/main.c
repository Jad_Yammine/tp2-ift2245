#include <stdlib.h>

#include "client_thread.h"

int
main (int argc, char *argv[])
{
    if (pthread_mutex_init(&mutex, NULL) != 0)
    {
        printf("\n mutex init failed\n");
        return 1;
    }

  sleep(4);
  printf("client started blabla 1\n");
  
  if (argc < 5) {
    fprintf (stderr, "Usage: %s <port-nb> <nb-clients> <nb-requests> <resources>...\n",
        argv[0]);
    exit (1);
  }
 
  port_number = atoi (argv[1]);
  //ct_share_parametres(num_clients,num_request_per_client,num_resources,provisioned_resources);
  ct_open_socket();
  sleep(2);
  num_clients = atoi (argv[2]);
  num_request_per_client = atoi (argv[3]);
  num_resources = argc - 4;

  provisioned_resources = malloc (num_resources * sizeof (int));
  for (unsigned int i = 0; i < num_resources; i++){
    provisioned_resources[i] = atoi (argv[i + 4]);
  }
  startup();
  
  client_thread *client_threads
    = malloc (num_clients * sizeof (client_thread));
  for (unsigned int i = 0; i < num_clients; i++){
    ct_init (&(client_threads[i]));
  }

      printf("avant create and start \n");

  for (unsigned int i = 0; i < num_clients; i++) {
    ct_create_and_start (&(client_threads[i]));
  }

  ct_wait_server ();
  print_clients();
  // Affiche le journal.
  st_print_results (stdout, true);
  FILE *fp = fopen("client.log", "w");
  if (fp == NULL) {
    fprintf(stderr, "Could not print log");
    return EXIT_FAILURE;
  }
  st_print_results (fp, false);
  fclose(fp);
   // pthread_mutex_destroy(&mutex);
  return EXIT_SUCCESS;
}
