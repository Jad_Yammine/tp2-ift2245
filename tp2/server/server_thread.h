#ifndef SERVER_THREAD_H
#define SERVER_THREAD_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//POSIX library for threads
#include <pthread.h>
#include <unistd.h>
pthread_mutex_t mutex;
pthread_mutex_t INI;
pthread_mutex_t CLO;

pthread_mutex_t ACK;

pthread_mutex_t WAIT;

pthread_mutex_t ERR;

extern bool accepting_connections;

typedef struct server_thread server_thread;
struct server_thread
{
  unsigned int id;
  pthread_t pt_tid;
  pthread_attr_t pt_attr;
};
void print_resources(int* x);
void print_clients();
void st_open_socket (int port_number);
void st_init (void);
void st_process_requests (server_thread *, int);
void st_signal (void);
void *st_code (void *);
//void st_create_and_start(st);
void st_print_results (FILE *, bool);
int st_wait();

#endif

