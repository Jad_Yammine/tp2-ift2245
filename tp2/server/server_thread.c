#define _XOPEN_SOURCE 700   /* So as to allow use of `fdopen` and `getline`.  */

#include "server_thread.h"

#include <netinet/in.h>
#include <netdb.h>

#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <signal.h>

#include <time.h>

#define ACK_BEG_ID 9999

//#define SERVER_SPACE = "                        "

enum {
    NUL = '\0'
};

enum {
    /* Configuration constants.  */
    max_wait_time = 30,
    server_backlog_size = 5
};

int server_socket_fd;

// Nombre de client enregistré.
int nb_registered_clients;
//int nb_registering_c = 0;

int nb_ressource;

// Variable du journal.
// Nombre de requêtes acceptées immédiatement (ACK envoyé en réponse à REQ).
unsigned int count_accepted = 0;

// Nombre de requêtes acceptées après un délai (ACK après REQ, mais retardé).
unsigned int count_wait = 0;

// Nombre de requête erronées (ERR envoyé en réponse à REQ).
unsigned int count_invalid = 0;

// Nombre de clients qui se sont terminés correctement
// (ACK envoyé en réponse à CLO).
unsigned int count_dispatched = 0;

// Nombre total de requête (REQ) traités.
unsigned int request_processed = 0;

// Nombre de clients ayant envoyé le message CLO.
unsigned int clients_ended = 0;

// TODO: Ajouter vos structures de données partagées, ici.
bool* client_status;

struct client_profile {
    int client_id;
    int current_nb_request;
    int *r_max_usage;
    int *r_taken;
};

struct client_profile *client_rep;

void print_clients() {
    printf("Printing registered clients:\n");
    for (int i = 0; i < nb_registered_clients; i++) {
        printf("Client id: >%d<\n", client_rep[i].client_id);
        print_resources(client_rep[i].r_max_usage);

    }
}

struct ressource_status {
    int *q;
    int lock;
};

struct ressource_status ressources;

void unlock_r() {
    ressources.lock = 0;
}

void lock_r() {
    ressources.lock = 1;
}

int try_lock_r() {
    if (ressources.lock == 0) {
        ressources.lock = 1;
        return 0;
    }
    return 1;
}

void print_resources(int* x) {
    printf("Printing ressources :\n");
    for (int i = 0; i < nb_ressource; i++) {
        printf("Ressource # %d contains: %d \n", i, x[i]);
    }
}


////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

void calculateNeed(int** need) {
    // Calculating Need of each P
    for (int i = 0; i < nb_registered_clients; i++)
        for (int j = 0; j < nb_ressource; j++)
            // Need of instance = maxm instance -
            //                    allocated instance
            need[i][j] = client_rep[i].r_max_usage[j] - client_rep[i].r_taken[j];

}

// Function to find the system is in safe state or not

bool isSafe(int* avail, int** allot) {
    int** need = malloc(nb_registered_clients * nb_ressource * sizeof (int));

    // Function to calculate need matrix
    calculateNeed(need);

    // Mark all processes as infinish
    bool finish[nb_registered_clients];
    for (int i = 0; i < nb_registered_clients; i++) {
        finish[i] = 0;
    }

    // To store safe sequence
    //int safeSeq[nb_registered_clients];

    // Make a copy of available resources
    int work[nb_ressource];
    for (int i = 0; i < nb_ressource; i++)
        work[i] = avail[i];

    // While all processes are not finished
    // or system is not in safe state.
    int count = 0;
    while (count < nb_registered_clients) {
        // Find a process which is not finish and
        // whose needs can be satisfied with current
        // work[] resources.
        bool found = false;
        for (int p = 0; p < nb_registered_clients; p++) {
            // First check if a process is finished,
            // if no, go for next condition
            if (finish[p] == 0) {
                // Check if for all resources of
                // current P need is less
                // than work
                int j;
                for (j = 0; j < nb_ressource; j++)
                    if (need[p][j] > work[j])
                        break;

                // If all needs of p were satisfied.
                if (j == nb_ressource) {
                    // Add the allocated resources of
                    // current P to the available/work
                    // resources i.e.free the resources
                    for (int k = 0; k < nb_ressource; k++)
                        work[k] += allot[p][k];

                    // Add this process to safe sequence.
                    //safeSeq[count++] = p;

                    // Mark this p as finished
                    finish[p] = 1;

                    found = true;
                }
            }
        }

        // If we could not find a next process in safe
        // sequence.
        if (found == false) {
            printf("System is not in safe state");
            return false;
        }
    }

    // If system is in safe state then
    // safe sequence will be as below
    //cout << "System is in safe state.\nSafe"
    //     " sequence is: ";

    printf("System is in safe state.");
    //for (int i = 0; i < P ; i++)
    //   cout << safeSeq[i] << " ";
    free(need);
    return true;
}





////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

bool
verify_req(int id, int *r) {
    int* new_available = malloc(nb_ressource * sizeof (int));
    for (int j = 0; j < nb_ressource; j++) {
        new_available[j] = ressources.q[j] - r[j];
    }
    int** new_allocation = malloc(nb_ressource * nb_registered_clients * sizeof (int));
    for (int i = 0; i < nb_registered_clients; i++) {
        new_allocation[i] = malloc(nb_ressource * sizeof (int));
        for (int j = 0; j < nb_ressource; j++) {
            if (i == id) {
                new_allocation[i][j] = client_rep[i].r_taken[j] + r[j];
            }
        }
    }

    return isSafe(new_available, new_allocation);
}



//return -2 for Not Enought ressources
//return -1 for Requested ressources > allowed Reqeuest
//return 1 if success

int request_ressource(int id, int *r) {

    //request_processed++;
    for (int j = 0; j < nb_ressource; j++) {
        if (client_rep[id].r_max_usage[j] < r[j]) {
            if (ressources.q[j] >= r[j]) {
                if (verify_req(id, r)) {
                    client_rep[id].r_taken[j] = client_rep[id].r_taken[j] + r[j];
                    ressources.q[j] = ressources.q[j] - r[j];
                } else {
                    return -2;
                }
            } else {
                return -2;
            }
        } else {
            return -1;
        }

    }
    return 1;
}

void add_client(int id, int *r_max) {
    struct client_profile x;

    //x.client_id = &(malloc(sizeof(int)));
    x.client_id = id;
    //x.r_max_usage = malloc(nb_ressource*sizeof(int));
    x.r_max_usage = r_max;
    //x.current_nb_request = malloc(sizeof(int));
    x.current_nb_request = -1;
    //x.r_taken = malloc(nb_ressource*sizeof(int));

    //printf("Server thread add client.\n");
    client_rep[id] = x;
    // printf("Server thread add client2.\n");

    //nb_registering_c++;
    // printf("Server thread add client3.\n");

}

void send_ACK(int mysocket) {

    //FILE *socket_w = fdopen (server_socket_fd, "w");
    //fprintf (socket_w, "ACK %d\n", id);
    char ans[4] = {'A', 'C', 'K', '\0'};

    //ans="ACK";
    send(mysocket, ans, strlen(ans), 0);


}

void send_WAIT(int mysocket, int wait_time) {
    char *ans = malloc((1024 * sizeof (char)));

    sprintf(ans, "WAIT %d", wait_time);
    send(mysocket, ans, strlen(ans), 0);
    free(ans);
}

void send_ERR(int mysocket, char* msg) {
    char *ans = malloc(1024 * sizeof (char));

    sprintf(ans, "ERR %s", msg);
    send(mysocket, ans, strlen(ans), 0);
    free(ans);
}

static void sigint_handler(int signum) {
    // Code terminaison.
    accepting_connections = 0;
    printf("BYE BYE!\n");
}

void println(void *data) {
    printf("Provisionned Ressource %s\n", (char*) data);
}

int* parse_resource(char* x) {
    //remove_first_space(x);
    char* one = strtok(x, " ");
    int *r;
    r = malloc(nb_ressource * sizeof (int));
    *r = atoi(one);
    for (int i = 1; i < nb_ressource; i++) {
        one = strtok(NULL, " ");
        if (strlen(one) != 0) {
            r[i] = atoi(one);

        } else {
            printf("Empty ressource was attempted to add to int\n");
        }
    }
    return r;

}

void
st_init() {
    // Handle interrupt
    signal(SIGINT, &sigint_handler);


    // Initialise le nombre de clients connecté.
    nb_registered_clients = 0;
    // TODO
    int mysocket = st_wait();
    //server_socket_fd = mysocket;
    char buff[1024] = {0};

    char *ans = malloc((1024 * sizeof (char)));
    //printf("Avant server read\n");
    read(mysocket, buff, (1024 * sizeof (char)));
    //printf("Server Thread BEG command received: >>%s<< \n",buff);
    if (buff[0] == 'B' && buff[1] == 'E' && buff[2] == 'G') {
        ans = "ACK";
        send(mysocket, ans, strlen(ans), 0);

        nb_ressource = atoi(strtok(buff + 4, " "));
        nb_registered_clients = atoi(strtok(NULL, " "));
        client_rep = malloc((nb_registered_clients)*(2 * sizeof (int) + 2 * nb_ressource * sizeof (int)));
        client_status = malloc(nb_registered_clients * sizeof (bool));
        for (int i = 0; i < nb_registered_clients; i++) {
            client_rep[i].client_id = -1;
            client_status[i] = true;
        }
        ressources.q = malloc(nb_ressource * sizeof (int));

        //printf("Server Thread BEG command interpreted: >>%d<< >>%d<<\n",nb_ressource, nb_registered_clients);

    } else {
        ans = "ERR pas une commande de depart valide";
        //send(mysocket , ans , strlen(ans) , 0 );

    }

    //creation structures banquier ici
    read(mysocket, buff, (1024 * sizeof (char)));
    //printf("Server Thread PRO command received:  >>%s<< \n",buff);
    if (buff[0] == 'P' && buff[1] == 'R' && buff[2] == 'O') {
        ans = "ACK";
        send(mysocket, ans, strlen(ans), 0);
        ressources.q = parse_resource(buff + 4);
        //print_resources(ressources.q);
        //printf("Server Thread end of PRO command.\n");

    } else {
        ans = "ERR pas une commande de depart valide";
        //send(mysocket , ans , strlen(ans) , 0 );

    }

    //free(ans);
    //fclose(server_socket_fd);
    //printf("Server Thread end of ST_INIT.\n");

    //creation structures banquier ici
    //send_WAIT(100);


    // Attend la connection d'un client et initialise les structures pour
    // l'algorithme du banquier.
    // END TODO

}

int
st_pro_REQ(char* buff) {

    int re = 0;
    if (buff[0] == 'R' && buff[1] == 'E' && buff[2] == 'Q') {
        int current_id = atoi(strtok(buff + 4, " "));
        int *req_q = parse_resource(strtok(NULL, "\n"));
        pthread_mutex_lock(&mutex);
        re = request_ressource(current_id, req_q);
        pthread_mutex_unlock(&mutex);
        free(req_q);
    }
    return re;

}

int
st_REQ_print(char* buff) {

    //int i = 0;
    if (buff[0] == 'R' && buff[1] == 'E' && buff[2] == 'Q') {

        // char *buff=malloc(1024);
        //read(socket_fd,buff,1024);
        // printf("Server REQ:  >>%s<<\n", buff);
        return 1;
        //free(buff);
    }
    return 0;
}

bool
client_exist(int id) {
    for (int i = 0; i < nb_registered_clients; i++) {
        if (client_rep[i].client_id == id) {
            return true;
        }
    }
    return false;
}

int
st_pro_INI(char* buff) {


    //printf("Server received on socket >>%d<< command: >>%s<< \n",socket_fd,buff);
    if (buff[0] == 'I' && buff[1] == 'N' && buff[2] == 'I') {
        //send_WAIT(socket_fd,1);
        //printf("Server thread INI command received: >>%s<< \n", buff);
        int current_id = atoi(strtok(buff + 4, " "));
        if (client_exist(current_id)) {
            return -1;
        } else {
            int *max_c_r = parse_resource(strtok(NULL, "\n"));

            //printf("Server_thread received INI command with following arguments: \n");
            //print_resources(max_c_r);
            //printf("Server thread INI after printing resources.\n");

            add_client(current_id, max_c_r);
            //count_dispatched++;
            free(max_c_r);
            return 1;
        }
        //send_ACK(socket_fd);
        //printf("Server thread INI command FINISHED.\n");


    } else {
        return 0;
    }
}

int
st_pro_CLO(char* buff) {
    if (buff[0] == 'C' && buff[1] == 'L' && buff[2] == 'O') {
        int current_id = atoi(strtok(buff + 4, " "));
        if (client_status[current_id] == false) {
            return -1;
        } else {
            client_status[current_id] = false;
            count_dispatched++;
            return 1;
        }
    } else {
        return 0;
    }
}

void
st_process_requests(server_thread * st, int socket_fd) {
    int i = 0;
    char *buff = malloc(1024 * sizeof (char));
    for (int f = 0; f < 1024; f++) {
        buff[f] = '\000';
    }
    read(socket_fd, buff, (1024 * sizeof (char)));
    printf("\n Server >>%u<<  received  >>%s<<\n", st->id, buff);
    i = st_pro_INI(buff);
    if (i == 1) {
        send_ACK(socket_fd);
        //close(socket_fd);
    } else if (i == -1) {
        send_ERR(socket_fd, "Server: Client already INITIALISED.");
    }

    int u = 0;
    u = st_pro_REQ(buff);

    if (u == 1) {
        send_ACK(socket_fd);
        //close(socket_fd);
    } else if (u == -2) {
        send_WAIT(socket_fd, 1);
    } else if (u == -1) {
        send_ERR(socket_fd, "Server Thread: Client request REFUSED, It exceeds max allowed ressources.\n");
    }


    int j = 0;
    j = st_pro_CLO(buff);
    if (j == 1) {
        send_ACK(socket_fd);
        //close(socket_fd);
    } else if (j == -1) {
        send_ERR(socket_fd, "Server Thread: Client request REFUSED, It exceeds max allowed ressources.\n");
    }

    free(buff);

    pthread_mutex_lock(&CLO);

    if (count_dispatched == nb_registered_clients) {
        accepting_connections = false;
        free(client_rep);
        close(server_socket_fd);
    }
    pthread_mutex_destroy(&CLO);
    pthread_mutex_unlock(&CLO);

    // TODO: Remplacer le contenu de cette fonction
}

int st_wait() {
    struct sockaddr_in thread_addr;
    socklen_t socket_len = sizeof (thread_addr);
    int thread_socket_fd = -1;
    int end_time = time(NULL) + max_wait_time;

    while (thread_socket_fd < 0 && accepting_connections) {
        thread_socket_fd = accept(server_socket_fd,
                (struct sockaddr *) &thread_addr,
                &socket_len);
        if (time(NULL) >= end_time) {
            break;
        }
    }
    return thread_socket_fd;
}

void *
st_code(void *param) {
    server_thread *st = (server_thread *) param;
    int thread_socket_fd = -1;
    // Boucle de traitement des requêtes.
    while (accepting_connections) {



        // Wait for a I/O socket.
        thread_socket_fd = st_wait();
   
        if (thread_socket_fd < 0) {
            fprintf(stderr, "Time out on thread %d.\n", st->id);
            continue;
        }

        if (thread_socket_fd > 0) {
            st_process_requests(st, thread_socket_fd);
            close(thread_socket_fd);
        }
    }
    return NULL;
}


//
// Ouvre un socket pour le serveur.
//

void
st_open_socket(int port_number) {
    server_socket_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (server_socket_fd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    if (setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof (int)) < 0) {
        perror("setsockopt()");
        exit(1);
    }

    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port_number);

    if (bind
        (server_socket_fd, (struct sockaddr *) &serv_addr,
        sizeof (serv_addr)) < 0)
        perror("ERROR on binding");

    listen(server_socket_fd, server_backlog_size);
}


//
// Affiche les données recueillies lors de l'exécution du
// serveur.
// La branche else ne doit PAS être modifiée.
//

void
st_print_results(FILE * fd, bool verbose) {
    if (fd == NULL) fd = stdout;
    if (verbose) {
        fprintf(fd, "\n---- Résultat du serveur ----\n");
        fprintf(fd, "Requêtes acceptées: %d\n", count_accepted);
        fprintf(fd, "Requêtes en attente: %d\n", count_wait);
        fprintf(fd, "Requêtes invalides: %d\n", count_invalid);
        fprintf(fd, "Clients : %d\n", count_dispatched);
        fprintf(fd, "Requêtes traitées: %d\n", request_processed);
    } else {
        fprintf(fd, "%d %d %d %d %d\n", count_accepted, count_wait,
                count_invalid, count_dispatched, request_processed);
    }
}
